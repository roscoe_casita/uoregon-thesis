from subprocess import PIPE, STDOUT
import subprocess
import time
import json
import sys

run_programs = ["BMR","SHD","NC-D", "NC-DO0","NC-DO1","NC-DO3"]
test_programs = [["KS", ["-i", ".asc"], ["-o", ".asc"]],
            ["MTM", ["", ".dat"], ["", ""]],
            ["BEGK", ["", ".p"], ["", ".d"]],
            ["BMR", ["", ".dat"], ["", ".dat"]],
            ["SHD", [" 0 ", ".dat"], ["", ".dat"]],
            ["NC-D", ["D", ".dat"], ["", ".dat"]],
            ["NC-DO0", ["0", ".dat"], ["", ".dat"]],
            ["NC-DO1", ["1", ".dat"], ["", ".dat"]],
            ["NC-DO2", ["2", ".dat"], ["", ".dat"]],
            ["NC-DO3", ["3", ".dat"], ["", ".dat"]],
            ["NC-BnB", ["B", ".dat"], ["", ".dat"]],
            ]

dat_to_p = ["convert", ["C", ".dat"], ["", ".p"]]
dat_to_asc = ["convert", ["C", ".dat"], ["", ".asc"]]

output_tester = ["convert", ["t",".dat"],["",".dat"]]



test_files = [
            ["M", ["20", "24", "28", "30", "32", "34", "36", "38", "40", "42", "44", "46","50","60","70","80","90","100"]],
            ["DM",  ["20", "24", "28", "30", "32", "34", "36", "38", "40", "42", "44", "46"]],
            ["TH", ["40", "60", "80", "100", "120", "140", "160", "180", "200"]],
            ["SDTH", ["42", "62", "82", "102", "122", "142", "162", "182", "202", "242", "282", "322", "362", "402"]],
            ["SDFP", ["9", "16", "23", "30", "37", "44", "51"]],
            ["win", ["100", "200", "400", "800", "1600", "3200", "6400", "12800", "25600", "44473"]],
            ["lose", ["100", "200", "400", "800", "1600", "3200", "6400", "12800", "16635"]],
            ["ac", ["200", "150", "130", "110", "90", "70", "50", "30"]]
            ]
test_files = [
                ["M", ["20", "24", "28", "30", "32", "34", "36", "38", "40", "42", "44", "46", "50", "60", "70", "80", "90", "100"]],
                ["DM", ["20", "24", "28", "30", "32", "34", "36", "38", "40", "42", "44", "46"]],
                ["TH", ["40", "60", "80", "100", "120", "140", "160", "180", "200"]],
                ["SDTH", ["42", "62", "82", "102", "122", "142", "162", "182", "202", "242", "282", "322", "362", "402"]],
                ["SDFP", ["9", "16", "23", "30", "37", "44", "51"]]
            ]

def run_a_test(tester, program, index,validate):
    name = tester["test_name"]
    items = tester["test_items"]
    test = items[index]

    inputfile = "data/" + name + "/" + name + test
    outputfile = "data/" + name + "/" + name + test
    command = program[0]

    ext = program[1][1]


    outputfile += "_" + command

    name = "Starting program " + command + " test " + name + test
    print(name)
    f = open('last_exec', 'w')
    f.write(name)
    f.close()
    time = runcmd(inputfile,outputfile,program)
    print("Program " + command + " ran " + name + test + " in " + str(time))

    if program[0] != "NC-DO3" and validate is True:

        print("Validating inverse")
        dualfile = inputfile + ".test.dat"
        runcmd(outputfile,dualfile, program)
        runcmd(inputfile,dualfile,output_tester);

    return time

def runcmd(inputfile,outputfile,command):

    cmd = "./" + command[0]
    iparam = command[1]
    oparam = command[2]


    arglist = [cmd]
    if iparam[0] != "":
        arglist.append(iparam[0])

    arglist.append(inputfile + iparam[1])

    if oparam[0] != "":
        arglist.append(oparam[0])

    start = time.time()

    if oparam[1] != "":
        arglist.append(outputfile + oparam[1])
        p = subprocess.Popen(arglist, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        if len(stderr) >0:
            print("Error in subproces:")
            print(str(stderr))

    else:
        f = open(outputfile + iparam[1],'w')
        p = subprocess.Popen(arglist, stdout=f, stderr=PIPE,shell=True)
        stdout, stderr = p.communicate()
        f.close()

    end = time.time()

    return end - start


def make_tester_from_test(files,programs):
    tester = dict()
    name = files[0]
    items = files[1]
    tester["test_name"] = name
    tester["test_items"] = items
    for [name,iparam,oparam] in programs:
        tester[name] = list()
        for i in items:
            tester[name].append(0.0)
    return tester

def readtester(testname):
    filename = testname + ".data"
    f = open(filename, 'r')
    tester = json.load(f)
    f.close()
    return tester

def writetester(tester):
    filename = tester["test_name"] + ".data"
    f = open(filename,'w')
    json.dump(tester,f)
    f.close()


def make_testers(test_ary,programs):
    return_value = list()
    for test in test_ary:
        tester = make_tester_from_test(test,programs)
        return_value.append(tester)
    return return_value

def save_testers(tester_ary):
    for tester in tester_ary:
        writetester(tester)

def load_testers(test_files):
    return_value = list()
    for test in test_files:
        [name,items] = test
        tester = readtester(name)
        return_value.append(tester)
    return return_value


def interprest_values(values):
    extracts = extract_non_repeats(values)
    expma = 1.0
    if len(extracts) == 0:
        expma = 0.0
    elif len(extracts) == len(values):
        expma = float('Inf')
    else:
        expma = calc_ratio_squares(extracts)
        if expma == float('nan'):
            expma = float('Inf')
        else:
            expma = extracts[len(extracts)-1] * expma

    return expma

def pick_a_tester(testers):
    return_tester = None
    expma = float('Inf')

    for tester in testers:
        items = tester["test_items"]
        name = tester["test_name"]
        for key in run_programs:
            runs = tester[key]
            value = interprest_values(runs)
            if value == 0.0:
                return tester
            elif value < expma:
                return_tester = tester
                expma = value

    return return_tester

def select_program(tester):
    test_program = None
    test_expma = float('Inf')
    for key in run_programs:
        runs = tester[key]
        expma = interprest_values(runs)
        if expma == 0.0:
            return key
        if test_expma > expma:
            test_expma = expma
            test_program = key
    return test_program

def calc_ratio_squares(values):
    calc = 1.0
    last_value = values[0]
    for v in values:
        ratio = v / last_value
        if ratio < 1.0 and v < 1.0:
            ratio = 1.0
        last_value = v
        calc = calc * ratio
    if last_value > 1.0:
        calc =calc * last_value
    return calc

def extract_non_repeats(runs):
    last_value = 0.0
    extract = list()
    for r in runs:
        if r == last_value:
            break
        extract.append(r)
        last_value = r
    return extract

def select_program_index(tester,program_name):
    index = 0
    runs = tester[program_name]
    last_value = 0.0
    for r in runs:
        if r == last_value:
            break
        last_value = r
        index+=1
    return index


def select_and_run(tester,programs,validate):
    program_name = select_program(tester)
    index = select_program_index(tester,program_name)
    program = None
    for prgm in programs:
        if prgm[0] == program_name:
            program = prgm
            break
    run_time = run_a_test(tester,program,index,validate)

    data =tester[program_name]

    for i in range(index,len(data)):
        data[i] = run_time


def run_one_test(testers,programs,validate):

    tester = pick_a_tester(testers)
    if tester == None:
        return True
    select_and_run(tester,programs,validate)
    return False

def reset(tester,item):
    for i in range(len(tester[item])):
        tester[item][i] = 0.0

def main2():
    testers = load_testers(test_files)
    items = ["NC-D","NC-DO0","NC-DO1","NC-DO2","NC-DO3"]
    for tester in testers:
        for item in items:
            reset(tester,item)
    save_testers(testers)

def main():
    done = False
    while done == False:
        testers = load_testers(test_files)
        done = run_one_test(testers,test_programs,False)
        save_testers(testers)

main()

