from subprocess import PIPE, STDOUT
import subprocess
import time
import json
import matplotlib.pyplot as plt
import itertools
test_datasets = [ "M", "DM", "TH", "SDTH", "SDFP", "win", "lose", "ac"]
bad_datasets = ["BMS"]

def make_tester_from_test(files,programs):
    tester = dict()
    name = files[0]
    items = files[1]
    tester["test_name"] = name
    tester["test_items"] = items
    for [name,iparam,oparam] in programs:
        tester[name] = list()
        for i in items:
            tester[name].append(float('Inf'))
    return tester

def fixvalues(yaxis):
    returnval = list()
    lastval = -1.0
    for val in yaxis:
        if val == lastval:
            returnval.append(float('Inf'))
        else:
            returnval.append(val)
            lastval = val
    return returnval

def showplot(tester,vals):

    xaxis = tester["test_items"]
    plot_name = tester["test_name"]


    for key in vals:
        yaxis = tester[key]
        yaxis = fixvalues(yaxis)

        if "-" not in key:
            plt.plot(xaxis,yaxis,label = key)
        else:
            lines = plt.plot(xaxis, yaxis, linestyle='--', label=key)


    plt.yscale('log')
    plt.legend()
    plt.title(plot_name)
    plt.ylabel('Time in seconds')
    plt.xlabel('Nodes / Problems')
    plt.show()

def readtester(testname):
    filename = testname + ".data"
    f = open(filename, 'r')
    tester = json.load(f)
    f.close()
    return tester

def load_testers(test_files):
    return_value = list()
    for test in test_files:
        tester = readtester(test)
        return_value.append(tester)
    return return_value


def main():
    bad_vals = ["NC-BnB", "MTM", "BMR","BEGK" , "NC-DO2", "NC-DO3"]

    vals = ["KS", "SHD", "NC-D", "NC-DO1", "NC-DO3"]
    testers = load_testers(test_datasets)
    for tester in testers:
        showplot(tester,vals)

    """showplot(tester,items)"""
main()
