

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>


#include <math.h>
#include <cstdlib>
#include <ctime>

size_t node_count =0;

size_t savetype = -1;

void LoadFrom_A_File(std::string filename,ExplicitHyperGraph<std::string> &load)
{
	std::ifstream infile(filename.c_str());

	int vars =0, domain =0;
	infile>>vars >> domain;

	std::string line;
	while(std::getline(infile,line))
	{
		if(line.compare("")!=0)
		{
			HyperEdge<std::string> hyperedge;
			std::stringstream reader(line);
			for(int counter=0; counter < vars; counter++)
			{
				domain=0;
				reader >> domain;
				if(domain == 1)
				{
					hyperedge.push_back(ItoS(counter+1));
				}
			}
			load.AddHyperEdge(hyperedge);
		}
	}
	infile.close();
	node_count = load.GetNodeCount();
}

std::string GenAFormatLine(size_t node_count,Odometer o)
{
	std::stringstream outstring;
	std::sort(o.begin(),o.end());
	size_t edge_index = 0;

	for(size_t counter=0; counter < node_count; counter++)
	{
		if(edge_index < o.size())
		{
			int val = o[edge_index];

			if(counter == (size_t)val)
			{
				edge_index++;
				outstring << "1 ";
			}
			else
			{
				outstring << "0 ";
			}
		}
		else
		{
			outstring << "0 ";
		}
	}
	outstring<<std::endl;
	return outstring.str();
}

std::string GenAFormat(ExplicitHyperGraph<std::string> &save)
{
	std::stringstream outfile;
	size_t node_count = save.GetNodeCount();
	size_t edge_count =save.GetHyperEdgeCount();
	outfile << node_count << " " << "1" << std::endl;
	for(size_t i=0;i<edge_count; i++)
	{
		Odometer o = save.GetHyperIndex(i);

		outfile << GenAFormatLine(node_count,o);
	}
	return outfile.str();
}



void LoadFrom_HT_File(std::string filename,ExplicitHyperGraph<std::string> &load)
{
	std::ifstream infile(filename.c_str());

	std::string line;
	while(std::getline(infile,line))
	{
		if((line.compare("")!=0)&&(line[0]!='#'))
		{
			HyperEdge<std::string> hyperedge;
			std::stringstream reader(line);
			size_t i =0;
			while(!reader.eof())
			{
				i++;
				char domain='*';
				reader >> domain;
				if(domain == '0')
				{
					hyperedge.push_back(ItoS(i));
				}
			}
			load.AddHyperEdge(hyperedge);
		}
	}
	infile.close();
	node_count = load.GetNodeCount();
}

std::string GenHTFormatLine(size_t node_count,Odometer o)
{
	std::stringstream outstring;
	std::sort(o.begin(),o.end());
	size_t edge_index = 0;

	for(size_t counter=0; counter < node_count; counter++)
	{
		if(edge_index < o.size())
		{
			int val = o[edge_index];

			if(counter == (size_t)val)
			{
				edge_index++;
				outstring << "0";
			}
			else
			{
				outstring << "*";
			}
		}
		else
		{
			outstring << "*";
		}
	}
	outstring<<std::endl;
	return outstring.str();
}

std::string GenHTFormat(ExplicitHyperGraph<std::string> &save)
{
	std::stringstream outfile;
	size_t node_count = save.GetNodeCount();
	size_t edge_count =save.GetHyperEdgeCount();
	outfile << "# Nodes: " << node_count << std::endl;
	outfile << "# Edges: " << edge_count << std::endl;
	for(size_t i=0;i<edge_count; i++)
	{
		Odometer o = save.GetHyperIndex(i);

		outfile << GenHTFormatLine(node_count,o);
	}
	return outfile.str();
}



void LoadFrom_DAT_File(std::string filename,ExplicitHyperGraph<std::string> &load)
{
	std::ifstream infile(filename.c_str());

	std::string line;
	int temp;
	while(std::getline(infile,line))
	{
		if(line.compare("")!=0)
		{
			HyperEdge<std::string> hyperedge;
			std::stringstream reader(line);
			while(!reader.eof())
			{
				temp = -1;
				reader >>temp;
				if(temp !=-1)
				{
					hyperedge.push_back(ItoS(temp));
				}
			}
			load.AddHyperEdge(hyperedge);
		}
	}
	infile.close();
	node_count = load.GetNodeCount();
}

std::string GenDATFormatLine(HyperEdge<std::string> edge)
{
	std::stringstream outstring;
	for(HyperEdge<std::string>::iterator itr = edge.begin(); itr != edge.end(); itr++)
	{
		outstring << (*itr) << " ";
	}
	outstring.seekp(-1,outstring.cur);
	outstring<<std::endl;
	return outstring.str();
}

std::string GenDATFormat(ExplicitHyperGraph<std::string> &save)
{
	std::stringstream outfile;
	for(size_t i=0;i<save.GetHyperEdgeCount(); i++)
	{
		Odometer o = save.GetHyperIndex(i);
		HyperEdge<std::string> edge = save.GetHyperEdge(o);

		outfile << GenDATFormatLine(edge);
	}
	return outfile.str();
}


std::ofstream *unnamed_filewriter = NULL;
void CloseFile()
{
	if(unnamed_filewriter !=NULL)
	{
		unnamed_filewriter ->close();
		delete unnamed_filewriter ;
		unnamed_filewriter=NULL;
	}
}

std::string GetExtension(std::string filename)
{
	std::string returnValue;
	bool done = false;
	size_t index = filename.size();
	while(!done)
	{
		if(index ==0)
		{
			done = true;
			break;
		}
		index--;
		char temp = filename[index];
		if('.' == temp)
		{
			done = true;
		}
		returnValue = temp + returnValue;
	}
	return returnValue;
}

size_t GetExtensionType(std::string filename)
{
	std::string extension = GetExtension(filename);
	size_t ext_type = -1;
	if(extension.compare(".d")==0 || extension.compare(".p")==0)
	{
		ext_type = 0;
	}
	else if(extension.compare(".asc")==0)
	{
		ext_type = 1;
	}
	else if(extension.compare(".dat")==0)
	{
		ext_type =2;
	}
	return ext_type;
}

void OpenFile(std::string filename)
{
	CloseFile();
	unnamed_filewriter = new std::ofstream(filename.c_str());

	savetype = GetExtensionType(filename);
}


std::string GenLine(Odometer transversal,HyperEdge<std::string> edge)
{
	std::string line_item;

	switch(savetype)
	{
	case 0:
		line_item = GenAFormatLine(node_count,transversal);
		break;
	case 1:
		line_item = GenHTFormatLine(node_count,transversal);
		break;
	case 2:
		line_item = GenDATFormatLine(edge);
		break;
	default:
		assert(0==0);
		assert(0==1);
		break;
	}
	return line_item;
}

void CollectTransversal(Odometer transversal,HyperEdge<std::string> edge)
{
	std::sort(edge.begin(),edge.end());
	std::sort(transversal.begin(),transversal.end());
	(*unnamed_filewriter) << GenLine(transversal,edge);
}

void CollectTransversal2(CompactMinimalTransversal &cmt,ExplicitHyperGraph<std::string> &save)
{
	std::vector<HyperEdge<std::string> > items;
	for(size_t i=0; i<cmt.size();i++)
	{
		HyperEdge<std::string> item = save.GetHyperEdge(cmt[i]);
		std::sort(item.begin(),item.end());
		items.push_back(item);
	}

	std::stringstream ss;

	ss << "[";
	for(size_t outer=0; outer<items.size();outer++)
	{
		ss << "[";
		HyperEdge<std::string> item = items[outer];
		for(size_t inner=0; inner<item.size();inner++)
		{
			ss<<item[inner];
			if(inner +1 <item.size())
			{
				ss << ",";
			}
		}
		ss << "]";
		if(outer+1 < items.size())
		{
			ss << ",";
		}
	}
	ss << "]" << std::endl;

	(*unnamed_filewriter) << ss.str();
}

void SaveHypergraph(ExplicitHyperGraph<std::string> &save)
{
	switch(savetype)
	{
	case 0:
		(*unnamed_filewriter) << GenAFormat(save);
		break;
	case 1:
		(*unnamed_filewriter) << GenHTFormat(save);
		break;
	case 2:
		(*unnamed_filewriter) << GenDATFormat(save);
		break;
	default:
		assert(0==0);
		assert(0==1);
		break;
	}
}

void LoadHypergraph(std::string filename,ExplicitHyperGraph<std::string> &load)
{
	size_t loadtype = GetExtensionType(filename);

	switch(loadtype)
	{
	case 0:
		::LoadFrom_A_File(filename,load);
		break;
	case 1:
		::LoadFrom_HT_File(filename,load);
		break;
	case 2:
		::LoadFrom_DAT_File(filename,load);
		break;
	default:
		assert(0==0);
		assert(0==1);
		break;
	}
}

bool comp(std::string test1,std::string test2)
{
	if(test1.compare(test2)==0)
		return true;
	return false;
}

int main(int argc, char *argv[])
{
	bool run =(argc == 4) || (argc == 5);
	if(!run)
	{
		std::cout << "Mode 1 Usage: <prgn> <N|B|D|O> <input> <output> " << std::endl;
		std::cout << "<prgn>: program name " << std::endl;
		std::cout << "<N|B|D|1|2>: Naive, Branch-and-Bound, Dynamic, Optimized, Optimized: Non-Enumerated" << std::endl;
		std::cout << "<input>: input file name: .p, .asc, .dat " << std::endl;
		std::cout << "<output>: output file name: .d, .asc, .dat " << std::endl;
		std::cout << "Mode 2 Usage: <prgn> <G> <NC> <EC> <outfile> " << std::endl;
		std::cout << "<G>: Generate Connected Hypergraph to stdout" << std::endl;
		std::cout << "<NC>: Node Count" << std::endl;
		std::cout << "<EC>: Edge Count (soft target) " << std::endl;
		std::cout << "Mode 3 Usage: <prgn> <C> <inputfile> <outfile>" << std::endl;
		std::cout << "<C>: convert hypergraph format" << std::endl;
		std::cout << "<input>: source file name: .p, .asc, .dat " << std::endl;
		std::cout << "<output>: destination file name: .p, .asc, .dat " << std::endl;

		return -1;
	}

	ExplicitHyperGraph<std::string> test,test2;
	int node_count=0;
	int edge_count=0;
	switch(argv[1][0])
	{
	case 'N':
	case 'n':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByNaiveAll(test,CollectTransversal);
		CloseFile();
		break;
	case 'B':
	case 'b':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByBranchAndBound(test,CollectTransversal);
		CloseFile();
		break;
	case 'D':
	case 'd':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByCMTDynamic(test,CollectTransversal );
		CloseFile();
		break;

	case '0':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByCMTDynamicOptimized0(test,CollectTransversal);
		CloseFile();
		break;
	case '1':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByCMTDynamicOptimized1(test,CollectTransversal);
		CloseFile();
		break;
		break;
	case '2':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByCMTDynamicOptimized2(test,CollectTransversal);
		CloseFile();
		break;
		break;

	case '3':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		TransversalsByCMTDynamicOptimized3(test,CollectTransversal2);
		CloseFile();
		break;

	case 'G':
	case 'g':
		node_count = StoI(argv[2]);
		edge_count = StoI(argv[3]);
		test = GenerateRandomHypergraph(node_count,edge_count);
		OpenFile(argv[4]);
		SaveHypergraph(test);
		CloseFile();
		break;

	case 'C':
	case 'c':
		LoadHypergraph(argv[2],test);
		OpenFile(argv[3]);
		SaveHypergraph(test);
		CloseFile();
		break;

	case 'T':
	case 't':
		LoadHypergraph(argv[2],test);
		LoadHypergraph(argv[3],test2);
		if(CompareHypergraphs<std::string>(test,test2,comp))
		{
			std::cout<<"CORRECT: HYPERGRAPHS ARE SAME." << std::endl;
			return 0;
		}
		else
		{
			std::cout<<"WRONG: HYPERGRAPHS ARE DIFFERENT." << std::endl;
			std::cerr<<"WRONG: HYPERGRAPHS ARE DIFFERENT." << std::endl;
			return 1;
		}
	}


	return 0;
}
