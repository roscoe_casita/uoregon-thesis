#ifndef HYPERGRAPH_TRANSVERSAL_FUNCTIONS_CPP
#define HYPERGRAPH_TRANSVERSAL_FUNCTIONS_CPP
#pragma once
#include <vector>
#include "Odometer.hpp"
#include "HyperGraph.hpp"

int IsMinimalTransversal(Odometer o, std::vector<Odometer> edges);


class Gamma
{
public:
	Gamma();
public:
	Odometer XMinusIntersection;
	Odometer Intersection;
};

enum CMTResult
{
	ContainsAtLeastOneBeta, ContainsOnlyAlphas, ContainsGammas
};

class IHGResult;

class IHGResult
{
public:
	IHGResult();
public:
	std::vector<Odometer> Alphas;
	std::vector<Odometer> Betas;
	std::vector<Gamma> Gammas;
	Odometer new_alpha_set;
	CMTResult result;
};

IHGResult IntersectCMTWithEdge(std::vector<Odometer> Transversals,Odometer o);

bool GenNextAlpha(IHGResult &result,Odometer &gen,CompactMinimalTransversal &next_frame, CompactMinimalTransversal &original_frame);
bool GenFirstGamma(IHGResult &result,Odometer &gen,CompactMinimalTransversal &next_frame);
bool GenNextGamma(IHGResult &result,Odometer &gen, CompactMinimalTransversal &next_frame);

bool MergeTransversal(std::vector<Odometer> source,std::vector<Odometer> test,std::vector<Odometer> &compaction_result);

std::vector<CompactMinimalTransversal> CondenseMinimalTransversals(std::vector<CompactMinimalTransversal> stack_items, CompactMinimalTransversal frame);
std::vector<CompactMinimalTransversal> MergeCMTLists(std::vector<CompactMinimalTransversal> stack_items, std::vector<CompactMinimalTransversal> add_items);
bool DoesCMTContainHittingTransversal(CompactMinimalTransversal new_cmt,CompactMinimalTransversal old_cmt);
std::vector<CompactMinimalTransversal> RemoveHittingTransversals(CompactMinimalTransversal new_node,CompactMinimalTransversal old_node);

class TransversalExtractor
{
public:
	size_t TotalOdometers;
	std::vector<Odometer> HittingOdometers;
	std::vector<Odometer> NonHittingOdometers;
	std::vector<Odometer> sizes;
	std::vector<Odometer> indexes;
};


std::vector<CompactMinimalTransversal> ProcessNewCMT2(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index);
std::vector<CompactMinimalTransversal> ProcessNewCMT1(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index);
std::vector<CompactMinimalTransversal> ProcessNewCMT0(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index);
std::vector<CompactMinimalTransversal> ProcessNewCMT(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index);


void ExtractTransversals(std::vector<Odometer> odometers,ExplicitHyperGraph<std::string> &ehg, void (*process_set)(Odometer o, HyperEdge<std::string> min_set));

#endif
