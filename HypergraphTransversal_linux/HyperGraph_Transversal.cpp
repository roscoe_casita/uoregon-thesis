#include <assert.h>
#include <algorithm>
#include <math.h>
#include <vector>
#include "Odometer.hpp"
#include "HyperGraph.hpp"
#include "HyperGraph_Transversal_Functions.hpp"


void TransversalsByNaiveAll(ExplicitHyperGraph<std::string> &ehg,void (*process_hitting_set)(Odometer o, HyperEdge<std::string> hit_set))
{
	Odometer current_set;
	const int node_count = ehg.GetNodeCount();
	current_set.push_back(0);

	while(current_set.size() > 0)
	{
		if(IsMinimalTransversal(current_set,ehg.GetOdometers())==0)
		{
			(*process_hitting_set)(current_set,ehg.GetHyperEdge(current_set));
		}
		int current = current_set[current_set.size()-1];
		int next = current +1;
		if(next < node_count)
		{
			current_set.push_back(next);
		}
		else
		{
			current_set.pop_back();
			if(current_set.size() > 0)
			{
				current_set[current_set.size()-1]++;
			}
		}
	}
}

void TransversalsByBranchAndBound(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set))
{
	Odometer current_set;
	const int node_count = ehg.GetNodeCount();
	current_set.push_back(0);

	while(current_set.size() >0)
	{
		switch(IsMinimalTransversal(current_set,ehg.GetOdometers()))
		{
		// Minimal transversal has been reached. produce output here.
		case 0:
			(*process_min_hitting_set)(current_set,ehg.GetHyperEdge(current_set));
			// DO NOT BREAK;.
		// Over hit is the same unwind / move forward logic as transversal being reached.
		case 1:
			// Move to the next index if you can.
			if(current_set[current_set.size()-1] < node_count -1)
			{
				current_set[current_set.size()-1]++;
			}
			else
			{
				// remove the current index, and move the next one if you can.
				current_set.pop_back();
				if(current_set.size() > 0)
				{
					current_set[current_set.size()-1]++;
				}
			}
			break;
		// Under hit, add a new item to the transversal.
		case -1:
			int next = current_set[current_set.size()-1] + 1;
			if(next < node_count)
			{
				current_set.push_back(next);
			}
			else
			{
				current_set.pop_back();
				if(current_set.size() > 0)
				{
					current_set[current_set.size()-1]++;
				}
			}
			break;
		}
	}
}



void TransversalsByCMTDynamicOptimized3(ExplicitHyperGraph<std::string> &ehg,
							void (*process_cmt)(CompactMinimalTransversal&,ExplicitHyperGraph<std::string> &))
{
	size_t hyperedge_count = ehg.GetHyperEdgeCount();

	CompactMinimalTransversal frame;

	std::vector<CompactMinimalTransversal> old_stack;
	std::vector<CompactMinimalTransversal> new_stack;



	frame.push_back( ehg.GetHyperIndex(0));
	old_stack.push_back(frame);


	for(size_t control =1; control < hyperedge_count; control++)
	{
		Odometer edge = ehg.GetHyperIndex(control);

		for(size_t os = 0; os < old_stack.size(); os++)
		{

			frame = old_stack[os];
			CompactMinimalTransversal new_cmt;

			IHGResult item_result = IntersectCMTWithEdge(frame,edge);
			Odometer alpha_counter;
			Odometer gamma_counter;


			switch(item_result.result)
			{
			case ContainsAtLeastOneBeta:
				new_stack = CondenseMinimalTransversals(new_stack,frame);
				break;
			case ContainsOnlyAlphas:
				new_cmt = frame;
				new_cmt.push_back( item_result.new_alpha_set);
				new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				break;
			case ContainsGammas:
				if(item_result.new_alpha_set.size()>0)
				{
					new_cmt = item_result.Alphas;
					new_cmt.push_back(item_result.new_alpha_set);
					for(size_t g =0; g < item_result.Gammas.size(); g++)
					{
						new_cmt.push_back(item_result.Gammas[g].XMinusIntersection);
					}
					new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				}
				for(size_t i =0; i < item_result.Gammas.size(); i++)
				{
					gamma_counter.push_back(0);
				}
				while(GenNextGamma(item_result,gamma_counter,new_cmt))
				{
					new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				}
				break;
			}
		}
		old_stack = new_stack;
		new_stack.clear();
		new_stack.empty();
	}
	for(size_t i =0; i < old_stack.size(); i++)
	{
		(*process_cmt)(old_stack[i],ehg);
	}
}
void TransversalsByCMTDynamicOptimized2(ExplicitHyperGraph<std::string> &ehg,
							void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set))
{
	size_t hyperedge_count = ehg.GetHyperEdgeCount();

	CompactMinimalTransversal frame;

	std::vector<CompactMinimalTransversal> old_stack;
	std::vector<CompactMinimalTransversal> new_stack;



	frame.push_back( ehg.GetHyperIndex(0));
	old_stack.push_back(frame);


	for(size_t control =1; control < hyperedge_count; control++)
	{
		Odometer edge = ehg.GetHyperIndex(control);

		for(size_t os = 0; os < old_stack.size(); os++)
		{

			frame = old_stack[os];
			CompactMinimalTransversal new_cmt;

			IHGResult item_result = IntersectCMTWithEdge(frame,edge);
			Odometer alpha_counter;
			Odometer gamma_counter;


			switch(item_result.result)
			{
			case ContainsAtLeastOneBeta:
				new_stack = CondenseMinimalTransversals(new_stack,frame);
				break;
			case ContainsOnlyAlphas:
				new_cmt = frame;
				new_cmt.push_back( item_result.new_alpha_set);
				new_stack = ProcessNewCMT2(new_cmt,old_stack,new_stack,os);
				break;
			case ContainsGammas:
				if(item_result.new_alpha_set.size()>0)
				{
					new_cmt = item_result.Alphas;
					new_cmt.push_back(item_result.new_alpha_set);
					for(size_t g =0; g < item_result.Gammas.size(); g++)
					{
						new_cmt.push_back(item_result.Gammas[g].XMinusIntersection);
					}
					new_stack = ProcessNewCMT2(new_cmt,old_stack,new_stack,os);
				}
				for(size_t i =0; i < item_result.Gammas.size(); i++)
				{
					gamma_counter.push_back(0);
				}
				while(GenNextGamma(item_result,gamma_counter,new_cmt))
				{
					new_stack = ProcessNewCMT2(new_cmt,old_stack,new_stack,os);
				}
				break;
			}
		}
		old_stack = new_stack;
		new_stack.clear();
		new_stack.empty();
	}
	for(size_t i =0; i < old_stack.size(); i++)
	{
		ExtractTransversals(old_stack[i],ehg,process_min_hitting_set);
	}
}
void TransversalsByCMTDynamicOptimized1(ExplicitHyperGraph<std::string> &ehg,
							void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set))
							//void (*process_tree)(CompactMinimalTransversal & , Odometer,ExplicitHyperGraph<T> &))
{
	size_t hyperedge_count = ehg.GetHyperEdgeCount();

	CompactMinimalTransversal frame;

	std::vector<CompactMinimalTransversal> old_stack;
	std::vector<CompactMinimalTransversal> new_stack;



	frame.push_back( ehg.GetHyperIndex(0));
	old_stack.push_back(frame);


	for(size_t control =1; control < hyperedge_count; control++)
	{
		Odometer edge = ehg.GetHyperIndex(control);

		for(size_t os = 0; os < old_stack.size(); os++)
		{

			frame = old_stack[os];
			CompactMinimalTransversal new_cmt;

			IHGResult item_result = IntersectCMTWithEdge(frame,edge);
			Odometer alpha_counter;
			Odometer gamma_counter;


			switch(item_result.result)
			{
			case ContainsAtLeastOneBeta:
				new_stack = CondenseMinimalTransversals(new_stack,frame);
				break;
			case ContainsOnlyAlphas:
				new_cmt = frame;
				new_cmt.push_back( item_result.new_alpha_set);
				new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				break;
			case ContainsGammas:
				if(item_result.new_alpha_set.size()>0)
				{
					new_cmt = item_result.Alphas;
					new_cmt.push_back(item_result.new_alpha_set);
					for(size_t g =0; g < item_result.Gammas.size(); g++)
					{
						new_cmt.push_back(item_result.Gammas[g].XMinusIntersection);
					}
					new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				}
				for(size_t i =0; i < item_result.Gammas.size(); i++)
				{
					gamma_counter.push_back(0);
				}
				while(GenNextGamma(item_result,gamma_counter,new_cmt))
				{
					new_stack = ProcessNewCMT1(new_cmt,old_stack,new_stack,os);
				}
				break;
			}
		}
		old_stack = new_stack;
		new_stack.clear();
		new_stack.empty();
	}
	for(size_t i =0; i < old_stack.size(); i++)
	{
		ExtractTransversals(old_stack[i],ehg,process_min_hitting_set);
	}
}

void TransversalsByCMTDynamicOptimized0(ExplicitHyperGraph<std::string> &ehg,
							void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set))
							//void (*process_tree)(CompactMinimalTransversal & , Odometer,ExplicitHyperGraph<T> &))
{
	size_t hyperedge_count = ehg.GetHyperEdgeCount();

	CompactMinimalTransversal frame;

	std::vector<CompactMinimalTransversal> old_stack;
	std::vector<CompactMinimalTransversal> new_stack;



	frame.push_back( ehg.GetHyperIndex(0));
	old_stack.push_back(frame);


	for(size_t control =1; control < hyperedge_count; control++)
	{
		Odometer edge = ehg.GetHyperIndex(control);

		for(size_t os = 0; os < old_stack.size(); os++)
		{

			frame = old_stack[os];
			CompactMinimalTransversal new_cmt;

			IHGResult item_result = IntersectCMTWithEdge(frame,edge);
			Odometer alpha_counter;
			Odometer gamma_counter;


			switch(item_result.result)
			{
			case ContainsAtLeastOneBeta:
				new_stack = CondenseMinimalTransversals(new_stack,frame);
				break;
			case ContainsOnlyAlphas:
				new_cmt = frame;
				new_cmt.push_back( item_result.new_alpha_set);
				new_stack = ProcessNewCMT0(new_cmt,old_stack,new_stack,os);
				break;
			case ContainsGammas:
				if(item_result.new_alpha_set.size()>0)
				{
					new_cmt = item_result.Alphas;
					new_cmt.push_back(item_result.new_alpha_set);
					for(size_t g =0; g < item_result.Gammas.size(); g++)
					{
						new_cmt.push_back(item_result.Gammas[g].XMinusIntersection);
					}
					new_stack = ProcessNewCMT0(new_cmt,old_stack,new_stack,os);
				}
				for(size_t i =0; i < item_result.Gammas.size(); i++)
				{
					gamma_counter.push_back(0);
				}
				while(GenNextGamma(item_result,gamma_counter,new_cmt))
				{
					new_stack = ProcessNewCMT0(new_cmt,old_stack,new_stack,os);
				}
				break;
			}
		}
		old_stack = new_stack;
		new_stack.clear();
		new_stack.empty();
	}
	for(size_t i =0; i < old_stack.size(); i++)
	{
		ExtractTransversals(old_stack[i],ehg,process_min_hitting_set);
	}
}

void TransversalsByCMTDynamic(ExplicitHyperGraph<std::string> &ehg,
							void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set))
							//void (*process_tree)(CompactMinimalTransversal & , Odometer,ExplicitHyperGraph<T> &))
{
	size_t hyperedge_count = ehg.GetHyperEdgeCount();

	CompactMinimalTransversal frame;

	std::vector<CompactMinimalTransversal> old_stack;
	std::vector<CompactMinimalTransversal> new_stack;



	frame.push_back( ehg.GetHyperIndex(0));
	old_stack.push_back(frame);

	
	for(size_t control =1; control < hyperedge_count; control++)
	{
		Odometer edge = ehg.GetHyperIndex(control);

		for(size_t os = 0; os < old_stack.size(); os++)
		{

			frame = old_stack[os];
			CompactMinimalTransversal new_cmt;

			IHGResult item_result = IntersectCMTWithEdge(frame,edge);
			Odometer alpha_counter;
			Odometer gamma_counter;


			switch(item_result.result)
			{
			case ContainsAtLeastOneBeta:
				new_stack = CondenseMinimalTransversals(new_stack,frame);
				break;
			case ContainsOnlyAlphas:
				new_cmt = frame;
				new_cmt.push_back( item_result.new_alpha_set);
				new_stack = ProcessNewCMT(new_cmt,old_stack,new_stack,os);
				break;
			case ContainsGammas:
				if(item_result.new_alpha_set.size()>0)
				{
					new_cmt = item_result.Alphas;
					new_cmt.push_back(item_result.new_alpha_set);
					for(size_t g =0; g < item_result.Gammas.size(); g++)
					{
						new_cmt.push_back(item_result.Gammas[g].XMinusIntersection);
					}
					new_stack = ProcessNewCMT(new_cmt,old_stack,new_stack,os);
				}
				for(size_t i =0; i < item_result.Gammas.size(); i++)
				{
					gamma_counter.push_back(0);
				}
				while(GenNextGamma(item_result,gamma_counter,new_cmt))
				{
					new_stack = ProcessNewCMT(new_cmt,old_stack,new_stack,os);
				}
				break;
			}
		}
		old_stack = new_stack;
		new_stack.clear();
		new_stack.empty();
	}
	for(size_t i =0; i < old_stack.size(); i++)
	{
		ExtractTransversals(old_stack[i],ehg,process_min_hitting_set);
	}
}
