
#ifndef HYPERGRAPH_TRANSVERSAL_H
#define HYPERGRAPH_TRANSVERSAL_H
#pragma once
#include <assert.h>
#include <algorithm>
#include <math.h>
#include <vector>
#include "Odometer.hpp"
#include "HyperGraph.hpp"
#include "HyperGraph_Transversal_Functions.hpp"

void TransversalsByNaiveAll(ExplicitHyperGraph<std::string> &ehg,void (*process_hitting_set)(Odometer o, HyperEdge<std::string> hit_set));

void TransversalsByBranchAndBound(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set));
void TransversalsByCMTDynamic(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set));
void TransversalsByCMTDynamicOptimized0(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set));
void TransversalsByCMTDynamicOptimized1(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set));
void TransversalsByCMTDynamicOptimized2(ExplicitHyperGraph<std::string> &ehg,void (*process_min_hitting_set)(Odometer o, HyperEdge<std::string> min_set));
void TransversalsByCMTDynamicOptimized3(ExplicitHyperGraph<std::string> &ehg,void  (*process_cmt)(CompactMinimalTransversal&,ExplicitHyperGraph<std::string> &));

#endif
