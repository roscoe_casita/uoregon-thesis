from subprocess import call
import subprocess
import time
import json
import matplotlib.pyplot as plt

algonames = ["BEGK", "KS", "Naive", "NC-BnB", "NC-Dynamic"]
datasums = dict()


def showplot():
    dataplot = dict()
    x = datasums["xaxis"]
    total = datasums["total"]
    items = len(x)
    for key in algonames:
        dataplot[key] = list()
        for i in range(0,items):
            dataplot[key].append(0.0)


    for key in algonames:
        algo = datasums[key]
        for i in range(0,items):
            dataplot[key][i] = algo[i] / total

    last = dataplot["Naive"][0]
    for i in range(1,len(dataplot["Naive"])):
        if last > dataplot["Naive"][i]:
            dataplot["Naive"][i]=last
        else:
            last = dataplot["Naive"][i]

    plt.ylim(0,30)
    for key in dataplot.keys():
        plt.plot(x,dataplot[key],label=key)
    plt.legend()
    plt.title('Random - Edge 10:1 Node - Hypergraph')
    plt.ylabel('Time in seconds')
    plt.xlabel('Number of nodes')
    plt.show()



def loadplot():
    f = open('workfile2','r')
    l = json.load(f)
    f.close()
    return l

def main():
    global datasums
    datasums = loadplot()
    showplot()

main()
