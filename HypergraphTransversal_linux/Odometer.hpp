#ifndef ODOMETER_H
#define ODOMETER_H
#pragma once
#include <assert.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <sys/time.h>
typedef unsigned long long timestamp_t;

timestamp_t get_timestamp();

void GenOutputAndUpdateTimeStamp(timestamp_t &last,std::string message);

/*
	Odometers is a vector of indexs,  each index corresponds 1 to 1 to a specific node for all hypergraphs. 
	
	Each Odometers instance represents a single linear integer programming variable. 

	The program is now able to explitily control the Odometer, save instances of the control, and modify the indexes on each iteration.  
	
	http://www.quickperm.org/odometers.php

	Odometers have the following features:
		
		Represents Current Combination
		Counts All Generations ("Distance")
		Sequentially Linked Digits or Wheels
		Expected Duplicate Digits
		Standalone and Shareable Readings
		Defines Upper and Lower Indices 
		Able to Set(n) & Distribute Readings
		Able to Reset()
		Able to Rewind()
		Able to Fast-Forward(n)
		Calculate the Nth Combination / Permutation without enumerating first.
*/
class Odometer : public std::vector<int>
{
};

typedef std::vector<Odometer> CompactMinimalTransversal;


Odometer Union( Odometer A,  Odometer B);
Odometer Unionize( std::vector<Odometer> singles);
Odometer Intersection( Odometer A,  Odometer B);
Odometer Minus( Odometer A,  Odometer B);
bool StrictEqual( Odometer A,  Odometer B);
bool SetEqual( Odometer A,  Odometer B);

//bool DoesACoverB( Odometer &A, Odometer &B);
bool DoesAHitB( Odometer A, Odometer B);
bool DoesACoverBorBCoverA( Odometer A, Odometer B);
bool DoesAHitAll( Odometer A, std::vector<Odometer> sets);

//std::vector<Odometer> IntersectCMTwithOdometer( std::vector<Odometer> &sets,  Odometer &intersecter);
//bool DoesAnyHitA( std::vector<Odometer> &test,  Odometer &set);

bool AreAllNodesCovered( Odometer &nodes, std::vector<Odometer> &edges);

Odometer GenerateOdometerMinusIndex(Odometer original_set, size_t index);
void GenerateCombinationCounters(std::vector<Odometer> enumerate, Odometer &counter,Odometer &dimentions);
bool IncrementCombinationCounter(Odometer &perm,Odometer dimensions);


void PrintOdometer( Odometer &o);
std::ostream &operator<<(std::ostream &printer, Odometer &o);
std::ostream &operator<<(std::ostream &printer, std::vector<Odometer> &odometers);

std::string ItoS(int integer);
int StoI(std::string str);
#endif
