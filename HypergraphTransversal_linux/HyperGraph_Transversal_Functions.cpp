
#include <assert.h>
#include <algorithm>
#include <math.h>
#include "Odometer.hpp"
#include "HyperGraph.hpp"
#include "HyperGraph_Transversal_Functions.hpp"

Gamma::Gamma()
{

}
IHGResult::IHGResult():result(ContainsGammas)
{

}


int IsMinimalTransversal(Odometer o, std::vector<Odometer> edges)
{
	if(false==DoesAHitAll(o,edges))
	{
		return -1;
	}

	for(size_t tc=0; tc < o.size(); tc++)
	{
		Odometer test = GenerateOdometerMinusIndex(o,tc);
		if(true==DoesAHitAll(test,edges))
		{
			return 1;
		}
	}
	return 0;
}


IHGResult IntersectCMTWithEdge(std::vector<Odometer> Transversals,Odometer o)
{
	IHGResult returnValue;
	returnValue.new_alpha_set = o;
	for(std::vector<Odometer>::iterator itr = Transversals.begin(); itr != Transversals.end(); itr++)
	{
		Odometer intersection = Intersection((*itr),o);
		if(0 == intersection.size())
		{
			// Alpha node type identified.
			returnValue.Alphas.push_back((*itr));
		}
		else if (intersection.size() >= (*itr).size())
		{
			// Beta type node identified,
			returnValue.Betas.push_back((*itr));
			returnValue.new_alpha_set = Minus(returnValue.new_alpha_set,(*itr));
		}
		else 
		{
			// Gamma type node, collect intersection and X \minus intersection.
			// Update the outsection Edge \minus (union of all intersections).
			Gamma gamma;
			gamma.Intersection = intersection;
			gamma.XMinusIntersection = Minus((*itr),intersection);
			returnValue.Gammas.push_back(gamma);
			returnValue.new_alpha_set = Minus(returnValue.new_alpha_set,intersection);
		}
	}
	if(returnValue.Betas.size() >0)
	{
		returnValue.result = ContainsAtLeastOneBeta;
	}
	else
	{
		if(returnValue.Gammas.size()==0)
		{
			returnValue.result = ContainsOnlyAlphas;
		}
		else
		{
			returnValue.result = ContainsGammas;
		}
	}
	return returnValue;
}

bool GenNextAlpha(IHGResult &result,Odometer &gen,CompactMinimalTransversal &next_frame, CompactMinimalTransversal &original_frame)
{
	assert(gen.size()==1);
	if(((size_t)(gen[0])) >= result.new_alpha_set.size())
	{
		return false;
	}
	next_frame = original_frame;
	Odometer o;
	o.push_back(result.new_alpha_set[gen[0]++]);
	next_frame.push_back(o);
	return true;
}

bool GenFirstGamma(IHGResult &result,Odometer &gen,CompactMinimalTransversal &next_frame)
{
	assert(gen.size()==1);
	if(((size_t)(gen[0])) >= result.new_alpha_set.size())
	{
		return false;
	}
	// the first index of the cross product is NO intersections,
	//	so if all edge \ (Union all intersections) = 0 ,  then this won't hit.
	// This is the only test we need to do to not generate this bad child.

	// start at the first index. if there are gammas there are intersections

	next_frame = result.Alphas;
	for(size_t g =0; g < result.Gammas.size(); g++)
	{
		next_frame.push_back(result.Gammas[g].XMinusIntersection);
	}

	Odometer o;
	o.push_back(result.new_alpha_set[gen[0]++]);
	next_frame.push_back(o);
	return true;
}

bool GenNextGamma(IHGResult &result,Odometer &gen, CompactMinimalTransversal &next_frame)
{
	Odometer sizes;
	for(size_t i =0; i < gen.size();i++)
	{
		sizes.push_back(2);  // bit vector, dimentional sizes = all 2.
	}
	if(false==IncrementCombinationCounter(gen,sizes)) // increment first, skiping all falses, handled by GenFirstGamma.
		return false;

	next_frame = result.Alphas;
	for(size_t g =0; g < gen.size(); g++)
	{
		if(gen[g] == 0)
		{
			next_frame.push_back(result.Gammas[g].XMinusIntersection);
		}
		else
		{
			next_frame.push_back(result.Gammas[g].Intersection);
		}
	}
	return true;
}


bool MergeTransversal(std::vector<Odometer> source,std::vector<Odometer> test,std::vector<Odometer> &compaction_result)
{
	// Only consider encoding that are the same size.
	if(source.size() != test.size())
	{
		return false;
	}

	int diff_count = 0;
	size_t s = 0;
	while(s<source.size())
	{
		bool match = false;
		size_t t = 0;
		while(t <test.size())
		{
			if(SetEqual(source[s],test[t]))
			{
				compaction_result.push_back(test[t]);
				test.erase(test.begin()+t);
				t--;
				match = true;
				break;
			}
			t++;
		}
		if(match)
		{
			source.erase(source.begin()+s);
			s--;
		}
		else
		{
			if(diff_count>0)
			{
				return false;
			}
			else
			{
				diff_count++;
			}
		}
		s++;
	}

	assert(source.size()==1);
	assert(test.size()==1);
	Odometer result = Union(source[0],test[0]);
	compaction_result.push_back(result);
	return true;
}

std::vector<CompactMinimalTransversal> CondenseMinimalTransversals(std::vector<CompactMinimalTransversal> stack_items, CompactMinimalTransversal frame)
{
	bool done = false;
	CompactMinimalTransversal compaction_holder = frame;

	while(!done)
	{
		std::vector<Odometer> compaction_result;

		int index = -1;
		done = true;
		for(size_t si = 0; si < stack_items.size(); si++)
		{
			compaction_result.clear();
			if(true == MergeTransversal(stack_items[si],compaction_holder,compaction_result))
			{
				compaction_holder = compaction_result;
				index = (int)si;
				done = false;
				break;
			}
		}
		if(!done)
		{
			stack_items.erase(stack_items.begin() + index);
		}
		else
		{
			stack_items.push_back(compaction_holder);
		}
	}
	return stack_items;
}

std::vector<CompactMinimalTransversal> MergeCMTLists(std::vector<CompactMinimalTransversal> stack_items, std::vector<CompactMinimalTransversal> add_items)
{
	for(size_t ai =0; ai < add_items.size(); ai++)
	{
		CompactMinimalTransversal cmt = add_items[ai];
		stack_items = CondenseMinimalTransversals(stack_items,cmt);
	}
	return stack_items;
}

CompactMinimalTransversal IntersectCMTwithOdometer(CompactMinimalTransversal cmt,Odometer o)
{
	CompactMinimalTransversal returnValue;

	for(size_t i =0; i< cmt.size();i++)
	{
		returnValue.push_back(Intersection(cmt[i],o));
	}
	return returnValue;
}

Odometer GenCombination(CompactMinimalTransversal enumerator, Odometer indexes)
{
	Odometer returnValue;
	for(size_t e=0; e < enumerator.size(); e++)
	{
		returnValue.push_back(enumerator[e][indexes[e]]);
	}

	return returnValue;
}

CompactMinimalTransversal MakePieceWiseTransversal(Odometer o)
{
	CompactMinimalTransversal returnValue;
	for(size_t t=0; t < o.size(); t++)
	{
		Odometer temp;
		temp.push_back(o[t]);
		returnValue.push_back(temp);
	}
	return returnValue;
}


CompactMinimalTransversal GenCorrectCMT(CompactMinimalTransversal enumerator,Odometer indexes,CompactMinimalTransversal saver,CompactMinimalTransversal alphas)
{
	CompactMinimalTransversal returnValue;
	Odometer o = GenCombination(enumerator,indexes);
	for(size_t t=0; t < o.size(); t++)
	{
		if(t < saver.size())
		{
			Odometer val = saver[t];
			val.push_back(o[t]);	// perform the union.
			returnValue.push_back(val);
		}
		else
		{
			Odometer val;
			val.push_back(o[t]);
			returnValue.push_back(val);
		}
	}
	for(size_t a=0; a < alphas.size(); a++ )
	{
		returnValue.push_back(alphas[a]);
	}
	return returnValue;
}

std::vector<CompactMinimalTransversal> RemoveHittingTransversals2(CompactMinimalTransversal new_node,CompactMinimalTransversal old_node,IHGResult new_items,IHGResult old_items)
{
	std::vector<CompactMinimalTransversal> returnValue;


	CompactMinimalTransversal tester;
	for(size_t g=0; g<old_items.Gammas.size();g++)
	{
		tester.push_back(old_items.Gammas[g].Intersection);
	}
	for(size_t b=0; b<old_items.Betas.size();b++)
	{
		tester.push_back(old_items.Betas[b]);
	}

	CompactMinimalTransversal enumerator;
	CompactMinimalTransversal saver;

	for(size_t g=0; g<new_items.Gammas.size();g++)
	{
		enumerator.push_back(new_items.Gammas[g].Intersection);
		saver.push_back(new_items.Gammas[g].XMinusIntersection);
	}
	if(new_items.Betas.size()>0)
	{
		for(size_t b=0; b<new_items.Betas.size();b++)
		{
			enumerator.push_back(new_items.Betas[b]);
		}
	}
	else
	{
		CompactMinimalTransversal new_transversal = saver;
		for(size_t a=0; a < new_items.Alphas.size(); a++)
		{
			new_transversal.push_back(new_items.Alphas[a]);
		}
		returnValue.push_back(new_transversal);
	}

	Odometer sizes;
	Odometer indexes;
	GenerateCombinationCounters(enumerator,indexes,sizes);

	bool done = false;
	while(!done)
	{
		Odometer hit_test = GenCombination(enumerator,indexes);
		bool hiting =DoesAHitAll(hit_test,tester);

		if(!hiting)
		{
			CompactMinimalTransversal new_transversal = GenCorrectCMT(enumerator,indexes,saver,new_items.Alphas);
			returnValue = CondenseMinimalTransversals(returnValue,new_transversal);
		}
		done = !IncrementCombinationCounter(indexes,sizes);
	}
	return returnValue;
}

std::vector<CompactMinimalTransversal> ProcessNewCMT2(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index)
{
	std::vector<CompactMinimalTransversal> transversals;
	transversals.push_back(new_node);

	for(size_t i=0; i < old_stack.size() && transversals.size()>0; i++)
	{
		if(i==index)
			continue;
		CompactMinimalTransversal old_cmt = old_stack[i];
		std::vector<CompactMinimalTransversal> next_transversals;

		for(size_t c=0; c <transversals.size();c++)
		{
			CompactMinimalTransversal new_cmt = transversals[c];

			IHGResult new_items = IntersectCMTWithEdge(new_cmt,Unionize(old_cmt));
			IHGResult old_items = IntersectCMTWithEdge(old_cmt,Unionize(new_cmt));

			if((new_items.Betas.size() + new_items.Gammas.size() >= old_cmt.size()) &&
				(old_items.Betas.size() + old_items.Gammas.size() == old_cmt.size()))
			{
				// then it is possible to generate odometers that hit.

				CompactMinimalTransversal cmt = transversals[c];
				std::vector<CompactMinimalTransversal> temp = RemoveHittingTransversals2(new_cmt,old_cmt,new_items,old_items);
				next_transversals = MergeCMTLists(next_transversals,temp);
			}
			else
			{
				next_transversals = CondenseMinimalTransversals(next_transversals,new_cmt);
			}
		}
		transversals = next_transversals;
	}
	new_stack = MergeCMTLists(new_stack,transversals);
	return new_stack;
}


std::vector<CompactMinimalTransversal> RemoveHittingTransversals(CompactMinimalTransversal new_node,CompactMinimalTransversal old_node)
{
	std::vector<CompactMinimalTransversal> returnValue;

	Odometer sizes;
	Odometer indexes;
	GenerateCombinationCounters(new_node,indexes,sizes);

//	Odometer u = Unionize(new_node);
//	CompactMinimalTransversal tester = IntersectCMTwithOdometer(old_node,u); // this speeds up the testing.

	bool done = false;
	while(!done)
	{
		Odometer transversal;
		for(size_t j=0; j < new_node.size(); j++)
		{
			int value = new_node[j][indexes[j]];
			transversal.push_back(value);
		}
		done = !IncrementCombinationCounter(indexes,sizes);
		bool hiting =DoesAHitAll(transversal,old_node);
		if(!hiting)
		{
			CompactMinimalTransversal new_transversal = MakePieceWiseTransversal(transversal);
			returnValue = CondenseMinimalTransversals(returnValue,new_transversal);
		}
	}
	return returnValue;
}


std::vector<CompactMinimalTransversal> ProcessNewCMT1(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index)
{
	std::vector<CompactMinimalTransversal> transversals;
	transversals.push_back(new_node);

	for(size_t i=0; i < old_stack.size() && transversals.size()>0; i++)
	{
		if(i==index)
			continue;
		CompactMinimalTransversal old_cmt = old_stack[i];
		std::vector<CompactMinimalTransversal> next_transversals;

		for(size_t c=0; c <transversals.size();c++)
		{
			CompactMinimalTransversal new_cmt = transversals[c];

			IHGResult new_items = IntersectCMTWithEdge(new_cmt,Unionize(old_cmt));
			IHGResult old_items = IntersectCMTWithEdge(old_cmt,Unionize(new_cmt));

			if((new_items.Betas.size() + new_items.Gammas.size() >= old_cmt.size()) &&
				(old_items.Betas.size() + old_items.Gammas.size() == old_cmt.size()))
			{
				// then it is possible to generate odometers that hit.

				//std::vector<CompactMinimalTransversal> temp = RemoveHittingTransversals(cmt,old_cmt);
				//MergeCMTLists(&next_transversals,&temp);
				CompactMinimalTransversal cmt = transversals[c];
				std::vector<CompactMinimalTransversal> temp = RemoveHittingTransversals(cmt,old_cmt);
				next_transversals = MergeCMTLists(next_transversals,temp);
			}
			else
			{
				next_transversals = CondenseMinimalTransversals(next_transversals,new_cmt);
			}

		}
		transversals = next_transversals;
	}
	new_stack = MergeCMTLists(new_stack,transversals);
	return new_stack;
}


std::vector<CompactMinimalTransversal> ProcessNewCMT0(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index)
{
	std::vector<CompactMinimalTransversal> transversals;
	transversals.push_back(new_node);

	for(size_t i=0; i < old_stack.size() && transversals.size()>0; i++)
	{
		if(i==index)
			continue;
		CompactMinimalTransversal old_cmt = old_stack[i];
		std::vector<CompactMinimalTransversal> next_transversals;

		for(size_t c=0; c <transversals.size();c++)
		{
			CompactMinimalTransversal cmt = transversals[c];
			std::vector<CompactMinimalTransversal> temp = RemoveHittingTransversals(cmt,old_cmt);
			next_transversals = MergeCMTLists(next_transversals,temp);
		}
		transversals = next_transversals;
	}
	new_stack = MergeCMTLists(new_stack,transversals);
	return new_stack;
}

std::vector<CompactMinimalTransversal> ProcessNewCMT(CompactMinimalTransversal new_node,std::vector<CompactMinimalTransversal> old_stack, std::vector<CompactMinimalTransversal> new_stack, size_t index)
{

	Odometer sizes;
	Odometer indexes;

	GenerateCombinationCounters(new_node,indexes,sizes);
	bool done = false;
	while(!done)
	{
		// extract each transversal from this compact minimal transversal.
		Odometer transversal;
		for(size_t j=0; j < new_node.size(); j++)
		{

			int value = new_node[j][indexes[j]];
			transversal.push_back(value);
		}
		done = !IncrementCombinationCounter(indexes,sizes);

		// next piecewise transversal is ready to process.

		bool add_transversal =true;

		for(size_t i=0; i < old_stack.size(); i++)
		{
			if(i == index)
				continue;
			CompactMinimalTransversal old_transversals = old_stack[i];

			if(DoesAHitAll(transversal,old_transversals))
			{
				add_transversal = false;
				break;
			}

		}
		if(add_transversal)
		{
			CompactMinimalTransversal new_transversal;
			for(size_t j=0; j < transversal.size(); j++)
			{
				Odometer o;
				o.push_back(transversal[j]);
				new_transversal.push_back(o);
			}
			new_stack = CondenseMinimalTransversals(new_stack,new_transversal);
		}
	}
	return new_stack;
}


void ExtractTransversals(std::vector<Odometer> odometers,ExplicitHyperGraph<std::string> &ehg, void (*process_set)(Odometer o, HyperEdge<std::string> min_set))
{
	Odometer sizes;
	Odometer indexes;
	GenerateCombinationCounters(odometers,indexes,sizes);

	bool done = false;
	while(!done)
	{
		Odometer transversal;
		for(size_t j=0; j < odometers.size(); j++)
		{
			int value = odometers[j][indexes[j]];
			transversal.push_back(value);
		}
		HyperEdge<std::string> edge = ehg.GetHyperEdge(transversal);
		(*process_set)(transversal,edge);
		done = !IncrementCombinationCounter(indexes,sizes);
	}
}
