
#ifndef HYPERGRAPH_GENERATORS_H
#define HYPERGRAPH_GENERATORS_H
#pragma once
#include <assert.h>
#include <algorithm>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include "HyperGraph.hpp"
#include "Odometer.hpp"
		

bool IsEdgeSubsetOrSupersetOfAnyEdge(const std::vector<Odometer> &hypergraph,const Odometer &o)
{
	for(size_t i=0;i<hypergraph.size();i++)
	{
		if(true==DoesACoverBorBCoverA(hypergraph[i],o))
		{
			return true;
		}
	}
	return false;
}


template<class T>
std::vector<ExplicitHyperGraph<T> *> *GenerateNextSimpleHypergraphChildren(ExplicitHyperGraph<T> *gen_children)
{
	std::vector<ExplicitHyperGraph<T> *> *returnValue = new std::vector<ExplicitHyperGraph<T> *>();

	size_t edges = gen_children->GetHyperEdgeCount();
	for(size_t edge = 0; edge < edges; edge++)
	{
		Odometer o = gen_children->RemoveByIndex(0);
		ExplicitHyperGraph<T> * workitem = new ExplicitHyperGraph<T>(*gen_children);

		if(o.size() >2)
		{
			std::vector<Odometer> add_edges;

			for(size_t i =0; i < o.size(); i++)
			{
				Odometer new_edge = GenerateOdometerMinusIndex(o,i);
				if(false==IsEdgeSubsetORSupersetofAnyEdge(workitem->GetOdometers(),new_edge))
				{
					add_edges.push_back(new_edge);
				}
			}
			for(std::vector<Odometer>::iterator itr = add_edges.begin(); itr != add_edges.end(); itr++)
			{
				workitem->AddHyperIndex((*itr));
			}
		}
		// Are all nodes covered in the hypergraph?
		if(Unionize(workitem->GetOdometers()).size() == workitem->GetNodeCount())
		{
			returnValue->push_back(workitem);
		}
		else
		{
			delete workitem;
		}
	}
	return returnValue;
}

template<class T>
void EnumerateAllHypergraphsOverNodes(HyperEdge<T> nodes, void (*visit_function)(ExplicitHyperGraph<T> *visited_set,int count) )
{
	std::vector<std::vector<ExplicitHyperGraph<T> *> *> WorkQueue;

	std::vector<ExplicitHyperGraph<T> *> *CurrentItems;

	ExplicitHyperGraph<T> *CurrentItem;

	CurrentItem = new ExplicitHyperGraph<T>();

	CurrentItem->AddHyperEdge(nodes);

	CurrentItems = new std::vector<ExplicitHyperGraph<T> *> ();
	CurrentItems->push_back(CurrentItem);
	WorkQueue.push_back(CurrentItems);

	int count =0;

	while(false==WorkQueue.empty())
	{
		CurrentItems = WorkQueue.back();
		WorkQueue.pop_back();
		CurrentItem = CurrentItems->back();
		CurrentItems->pop_back();
		if(CurrentItems->size()>0)
		{
			WorkQueue.push_back(CurrentItems);
		}
		else
		{
			delete CurrentItems;
		}
		(*visit_function)(CurrentItem,count++);
		CurrentItems = GenerateNextSimpleHypergraphChildren(CurrentItem);
		if(CurrentItems->size()>0)
		{
			WorkQueue.push_back(CurrentItems);
		}
		else
		{
			delete CurrentItems;
		}
		delete CurrentItem;
	}
}



bool GenNewEdges(std::vector<Odometer> *edge_list,Odometer nodes)
{
	bool returnValue = false;
	srand(stime(0));

	std::vector<Odometer> temp = (*edge_list);
	edge_list->clear();

	while((returnValue==false) && (temp.size()>0))
	{
		int index = rand() % temp.size();
		Odometer item =temp[index];
		temp.erase(temp.begin() + index);

		if(item.size()>2)
		{
			for(size_t i=0; i < item.size();i++)
			{
				Odometer new_edge = GenerateOdometerMinusIndex(item,i);
				if(false==IsEdgeSubsetOrSupersetOfAnyEdge(temp,new_edge))
				{
					if(false==IsEdgeSubsetOrSupersetOfAnyEdge(*edge_list,new_edge))
					{
						edge_list->push_back(new_edge);
						returnValue = true;
					}
				}
			}
		}
		else
		{
			edge_list->push_back(item);
		}
	}
	for(size_t i=0;i<temp.size();i++)
	{
		edge_list->push_back(temp[i]);
	}

	while(edge_list->size()>0)
	{
		int index = rand()%edge_list->size();
		Odometer o = (*edge_list)[index];
		temp.push_back(o);
		edge_list->erase(edge_list->begin()+index);
	}
	while(temp.size()>0)
	{
		int index = rand()%temp.size();
		Odometer o = temp[index];
		edge_list->push_back(o);
		temp.erase(temp.begin()+index);
	}
	return returnValue;
}

ExplicitHyperGraph<std::string> GenerateRandomHypergraph(int node_count,int edge_count)
{
	HyperEdge<std::string> nodes;
	Odometer edge;
	std::vector<Odometer> edge_list;

	for(int i=0; i < node_count; i++)
	{
		edge.push_back(i);
		nodes.push_back(ItoS(i+1));

	}
	for(size_t i=0; i < edge.size();i++)
	{
		Odometer new_edge = GenerateOdometerMinusIndex(edge,i);
		edge_list.push_back(new_edge);
	}
	bool done = false;
	while(!done)
	{
		if(edge_list.size() >= (size_t)edge_count)
		{
			done = true;
			break;
		}
		if(GenNewEdges(&edge_list,edge) == false)
		{
			done =true;
			break;
		}
	}

	return ExplicitHyperGraph<std::string>(nodes,edge_list);
}

#endif
