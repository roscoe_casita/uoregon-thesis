#include "Odometer.hpp"
#include <iostream>
#include <assert.h>
#include <algorithm>

timestamp_t get_timestamp()
{
	struct timeval now;
	gettimeofday(&now,NULL);
	return now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}

std::string formattime(timestamp_t time)
{
	double seconds = (time)/ 1000000.0L;
	double miliseconds = (time % 1000000)/ 1000.0L;
	double microseconds = time % 1000;

	std::stringstream ss;
	ss << (int)seconds << ":" << (int)miliseconds << ":" << (int)microseconds;
	return ss.str();
}

void GenOutputAndUpdateTimeStamp(timestamp_t &last,std::string message)
{
	timestamp_t now = get_timestamp();
	timestamp_t diff = now -last;
	last = now;
	std::cout << message << " : " << formattime(diff) << std::endl;
}


Odometer Union(Odometer A, Odometer B)
{
	Odometer returnValue;
	returnValue=B;
	for(size_t i=0; i < A.size(); i++)
	{
		bool add =true;
		for(size_t j=0; j < B.size(); j++)
		{
			if(A[i] == B[j])
			{
				add = false;
				break;
			}
		}
		if(true==add)
		{
			returnValue.push_back(A[i]);
		}
	}
	return returnValue;
}

Odometer Unionize(std::vector<Odometer> singles)
{
	Odometer returnValue;

	size_t size = singles.size();

	for(size_t s=0; s < size; s++)
	{
		returnValue = Union(returnValue,singles[s]);
	}
	return returnValue;
}

Odometer Intersection(Odometer A, Odometer B)
{
	size_t outer_size = A.size();
	size_t inner_size = B.size();
	//assert(outer_size!=0);
	//assert(inner_size!=0);
	Odometer returnValue;
	for(size_t o=0; o < outer_size; o++)
	{
		int compare = A[o];
		for(size_t i=0; i < inner_size; i++)
		{
			if(compare == B[i])
			{
				returnValue.push_back(compare);
			}
		}
	}
	return returnValue;
}

Odometer Minus(Odometer A, Odometer B)
{
	size_t outer_size = A.size();
	size_t inner_size = B.size();

	Odometer returnValue;
	for(size_t o=0; o < outer_size; o++)
	{
		bool add =true;
		int compare = A[o];
		for(size_t i=0; i < inner_size; i++)
		{
			if(compare == B[i])
			{
				add = false;
				break;
			}
		}
		if(true == add)
		{
			returnValue.push_back(compare);
		}
	}	
	return returnValue;
}


bool StrictEqual(Odometer A, Odometer B)
{
	if(A.size()!=B.size())
		return false;
	size_t size = A.size();
	for(size_t i=0;i<size;i++)
	{
		if(A[i]!=B[i])
			return false;
	}
	return true;
}

bool SetEqual(Odometer A, Odometer B)
{
	if(A.size()!=B.size())
		return false;
	Odometer a = A;
	Odometer b = B;
	std::sort(a.begin(),a.end());
	std::sort(b.begin(),b.end());
	return StrictEqual(a,b);
}



bool DoesACoverB(Odometer A,Odometer B)
{
	size_t a_size = A.size();
	size_t b_size = B.size();
	for(size_t a = 0; a < a_size; a++)
	{
		bool found = false;
		for(size_t b =0; b < b_size;b++)
		{
			if(A[a]==B[b])
			{
				found = true;
				break;
			}
		}
		if(false==found)
			return false;
	}
	return true;
}

bool DoesACoverBorBCoverA(Odometer A,Odometer B)
{
	if(DoesACoverB(A,B))
		return true;
	if(DoesACoverB(B,A))
		return true;
	return false;
}

bool DoesAHitB(Odometer A,Odometer B)
{
	size_t a_size = A.size();
	size_t b_size = B.size();
	for(size_t a =0; a < a_size; a++)
	{
		for(size_t b =0; b<b_size; b++)
		{
			if(A[a]==B[b])
				return true;
		}
	}
	return false;
}

bool DoesAHitAll(Odometer A,std::vector<Odometer> sets)
{
	bool returnValue = true;
	size_t sets_size = sets.size();
	for(size_t ss = 0; ss < sets_size; ss++)
	{
		if(false==DoesAHitB(A,sets[ss]))
		{
			returnValue = false;
			break;
		}
	}
	return returnValue;
}

bool DoesAnyHitA(std::vector<Odometer> test, Odometer set)
{
	bool returnValue = false;
	size_t test_size = test.size();
	for(size_t ts = 0; ts < test_size; ts++)
	{
		if(true==DoesAHitB(test[ts],set))
		{
			returnValue=true;
			break;
		}
	}
	return returnValue;
}

Odometer GenerateOdometerMinusIndex(Odometer original_set, size_t index)
{
	Odometer returnValue;
	for(size_t i=0; i< original_set.size(); i++)
	{
		if(i != index)
		{
			returnValue.push_back(original_set[i]);
		}
	}
	return returnValue;
}

bool IsMinimalHitting(Odometer o, std::vector<Odometer> edges)
{
	if(false==DoesAHitAll(o,edges))
	{
		return false;
	}
	for(size_t tc=0; tc < o.size(); tc++)
	{
		Odometer test = GenerateOdometerMinusIndex(o,tc);
		if(true==DoesAHitAll(test,edges))
		{
			return false;
		}
	}
	return true;
}

void GenerateCombinationCounters(std::vector<Odometer> enumerate, Odometer &counter,Odometer &dimentions)
{
	for(size_t i = 0; i < enumerate.size(); i++)
	{
		counter.push_back(0);
		dimentions.push_back(enumerate[i].size());
	}
}

bool IncrementCombinationCounter(Odometer &counter,Odometer dimensions)
{
	int control =0;
	while(control < (int)counter.size())
	{
		if(counter[control]+1 < dimensions[control])
		{
			counter[control]++;
			return true;
		}
		else
		{
			counter[control]=0;
			control++;
		}
	}
	return false;
}

std::vector<Odometer> ExtractTransversals(std::vector<Odometer> odometers)
{
	std::vector<Odometer> returnValue;

	Odometer sizes;
	Odometer indexes;
	GenerateCombinationCounters(odometers,indexes,sizes);

	bool done = false;
	while(!done)
	{
		Odometer transversal;
		for(size_t j=0; j < odometers.size(); j++)
		{
			int value = odometers[j][indexes[j]];
			transversal.push_back(value);
		}
		returnValue.push_back(transversal);
		done = !IncrementCombinationCounter(indexes,sizes);
	}
	return returnValue;
}

void PrintOdometer(const Odometer &o)
{
	std::cout << "Odometer:";
	for(size_t i = 0; i < o.size(); i++)
	{
		std::cout << "\t" << o[i];
	}
	std::cout<< std::endl;
}

std::ostream &operator<<(std::ostream &printer,const Odometer &o)
{
	printer << "{" ;
	size_t size = o.size();
	for(size_t i = 0; i < size; i++)
	{
		printer << o[i];
		if(i +1 < size)
		{
			printer << "|";
		}
	}
	printer << "}" ;
	return printer;
}


std::ostream &operator<<(std::ostream &printer,const std::vector<Odometer> &odometers)
{
	printer << "[" ;
	size_t size = odometers.size();
	for(size_t i = 0; i < size; i++)
	{
		printer << odometers[i];
		if(i +1 < size)
		{
			printer << ",";
		}
	}
	printer << "]" ;
	return printer;
}

std::string ItoS(int integer)
{
	std::stringstream ss;
	ss<<integer;
	return ss.str();
}

int StoI(std::string str)
{
	std::stringstream ss(str);
	int returnValue;
	ss >> returnValue;
	return returnValue;
}
